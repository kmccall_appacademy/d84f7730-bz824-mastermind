class Code
  attr_reader :pegs

  PEGS = { 'r' => :red, 'g' => :green, 'b' => :blue, 'y' => :yellow,
        'o' => :orange, 'p' => :purple }

  def initialize(pegs)
    raise unless pegs.is_a?(Array)
    @pegs = pegs
  end

  def self.random
    gen = []
    4.times do
      gen << PEGS[rand(0..5)]
    end
    Code.new(gen)
  end

  def [](index)
    @pegs[index]
  end

  def self.parse(input)
    guess_code = input.downcase.split('')
    guess_code.each {|ch| raise unless PEGS.keys.include?(ch)}
    Code.new(guess_code)
  end

  def exact_matches(guess)
    matches = 0

    (0..3).each do |idx|
      matches += 1 if guess[idx] == pegs[idx]
    end

    matches
  end

  def near_matches(guess)
    matches = 0
    dup = pegs
    (0..3).each do |idx|
      if dup.include?(guess[idx]) && pegs[idx] != guess[idx]
        matches += 1
        dup[dup.index(guess[idx])] = "1"
      end
    end
    matches
  end

  def ==(guess)
    return false unless guess.is_a? Code
    exact_matches(guess) == 4
  end


end

class Game
  attr_reader :secret_code
  attr_accessor :get_guess

  def initialize(secret_code = nil)
    if secret_code.nil?
      @secret_code = Code.random
    else
      @secret_code = secret_code
    end
  end

  def get_guess
    puts "Guess the 4 colors. One letter for each color - no spaces, no symbols."
    guess = $stdin.gets.chomp()
    Code.parse(guess)
  end

  def display_matches(code)
    puts "exact matches: #{secret_code.exact_matches(code)}"
    puts "near matches: #{secret_code.near_matches(code)}"
  end
end
